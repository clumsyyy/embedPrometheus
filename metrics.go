package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/prometheus/client_golang/prometheus"
)

func CollectAndPrintMetrics(reg *prometheus.Registry) {
	metrics, err := reg.Gather()
	if err != nil {
		fmt.Printf("error on reg.Gather(): %s", err.Error())
	}

	fmt.Println("len of metrics: ", len(metrics))
	os.Exit(0)

	//fmt.Printf("metrics: %d\n", len(metrics))

	// Metrics hierarchy
	// Metric
	//		Name
	//		Metrics[]
	for _, v := range metrics {

		data, err := json.Marshal(v)
		if err != nil {
			fmt.Printf("%s\n", err.Error())
		}
		fmt.Printf("%v\n", string(data))
		/*
			x := v.Metric[0]
			items := x.Label
			y := items[0]
			y.Name
			y.Value
		*/
		/*
				var sb strings.Builder
				sb.WriteString(*v.Name + "\n")

				items := v.Metric
				for _, v2 := range items {
					labels := v2.GetLabel()
					for _, pair := range labels {
						sb.WriteString("\t" + *pair.Name + ", " + *pair.Value + "\n")
					}
						if v2.Counter != nil {
							sb.WriteString(fmt.Sprintf("\t\tCounter: %0.4f\n", *v2.Counter.Value))
						}
						if v2.Gauge != nil {
							sb.WriteString(fmt.Sprintf("\t\tGauge: %0.4f\n", *v2.Gauge.Value))
						}
				}
			fmt.Printf("%s", sb.String())
		*/
	}

}
