package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/alecthomas/kingpin/v2"
	kitlog "github.com/go-kit/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/node_exporter/collector"
	chronyCollector "github.com/superq/chrony_exporter/collector"
)

func main() {
	copiedArgs := os.Args
	os.Args = []string{copiedArgs[0]}

	// required to initialize the node_exporter collectors
	kingpin.Parse()

	os.Args = copiedArgs
	var mode string
	flag.StringVar(&mode, "mode", "", "something")
	flag.Parse()
	fmt.Printf("Mode: %s\n", mode)

	//pid := os.Getpid()
	//log.Printf("pid = %d\n", pid)

	reg := prometheus.NewRegistry()

	//logger := kitlog.NewNopLogger()
	logger := kitlog.NewJSONLogger(os.Stdout)

	chronyExporter := chronyCollector.NewExporter(
		chronyCollector.ChronyCollectorConfig{
			Address:            "[::1]:323",
			Timeout:            time.Duration(time.Second * 5000),
			ChmodSocket:        true,
			DNSLookups:         true,
			CollectSources:     true,
			CollectTracking:    true,
			CollectServerstats: true,
		},
		logger,
	)
	reg.MustRegister(chronyExporter)

	reg.MustRegister(collectors.NewGoCollector(), collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}))
	nc, err := collector.NewNodeCollector(logger)
	if err != nil {
		log.Fatal(err)
	}

	for _ = range nc.Collectors {
		//fmt.Printf("%s\n", key)
	}

	err = reg.Register(nc)
	if err != nil {
		fmt.Println(err)
	}
	browser(reg)

	terminal(reg)

}

func terminal(reg *prometheus.Registry) {
	counter := 0
	for {
		//s1 := time.Now()
		CollectAndPrintMetrics(reg)
		//s2 := time.Now()

		//fmt.Printf(
		//	"Counter: %d, elapsed milliseconds: %d\n\n\n",
		//	counter, s2.UnixMilli()-s1.UnixMilli(),
		//)
		counter++
		time.Sleep(time.Second * 5)
	}

}

func browser(reg *prometheus.Registry) {
	log.Printf("Starting web server for /metrics on :8080")
	http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}))
	log.Fatal(http.ListenAndServe(":8080", nil))
}

/*
func main() {
	kingpin.Parse()

	//flag.Parse()
	logger := kitlog.NewJSONLogger(os.Stdout)

	// Create non-global registry.
	reg := prometheus.NewRegistry()

	//cpucollector, err := collector.NewXFSCollector(logger)
	_, err := collector.NewCPUCollector(logger)
	if err != nil {
		log.Printf("NewCPUCollector error: %s", err.Error())
	}

	nc, err := collector.NewNodeCollector(logger)
	if err != nil {
		log.Fatalf("NewNodeCollector error: %s", err.Error())
	}

	// Add go runtime metrics and process collectors.
	reg.MustRegister(
		collectors.NewGoCollector(),
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
		nc,
	)

	// Expose /metrics HTTP endpoint using the created custom registry.
	log.Printf("Starting web server for /metrics on :8080")
	http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}))
	log.Fatal(http.ListenAndServe(":8080", nil))
}*/
